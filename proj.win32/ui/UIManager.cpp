#include "UIManager.h"
#include "BeltUI.h"

UIManager* UIManager::instance = UIManager::create();

UIManager::UIManager()
{}

UIManager* UIManager::create()
{
	UIManager::instance = new UIManager();

	//add belt UI
	BeltUI::create();

	return UIManager::instance;
}

UIManager* UIManager::getInstance()
{
	return UIManager::instance;
}

