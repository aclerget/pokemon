#include "Pokemon.h"
#include <string>
#include <utility>


#pragma region CONSTRUCTORS

Pokemon::Pokemon(std::string name = "", int pv = 0, int atk = 0, int def = 0, int speed = 0, int atkSpec = 0, int defSpec = 0, int precision = 0, int dodge = 0)
{
	this->id = 0;
	this->name = name;
	this->pv = pv;
	this->atk = atk;
	this->def = def;
	this->speed = speed;
	this->atkSpec = atkSpec;
	this->defSpec = defSpec;
	this->precision = precision;
	this->dodge = dodge;

	//transform name to lowercase
	std::transform(name.begin(), name.end(), name.begin(), ::toupper);
	this->miniThumb = Sprite::create("characters/pokemons/mini_"+name+".png");
	this->mediumThumb = nullptr;
	this->fightFace	= nullptr;
	this->fightBack	= nullptr;
}

Pokemon::Pokemon(int id, std::string name = "", int pv = 0, int atk = 0, int def = 0, int speed = 0, int atkSpec = 0, int defSpec = 0, int precision = 0, int dodge = 0)
{
	this->id = id;
	this->name = name;
	this->pv = pv;
	this->atk = atk;
	this->def = def;
	this->speed = speed;
	this->atkSpec = atkSpec;
	this->defSpec = defSpec;
	this->precision = precision;
	this->dodge = dodge;
	this->miniThumb = nullptr;
	this->mediumThumb = nullptr;
	this->fightFace = nullptr;
	this->fightBack = nullptr;
}

Pokemon::~Pokemon()
{
	this->miniThumb = nullptr;
	this->mediumThumb = nullptr;
	this->fightFace = nullptr;
	this->fightBack = nullptr;
	delete(this->miniThumb);
	delete(this->mediumThumb);
	delete(this->fightFace);
	delete(this->fightBack);
}

#pragma endregion

#pragma region GETTERS/SETTERS

int Pokemon::getId()
{
	return this->id;
}

void Pokemon::setId(int id)
{
	this->id = id;
}

std::string Pokemon::getName()
{
	return this->name;
}

void Pokemon::setName(std::string name)
{
	this->name = name;
}

int Pokemon::getPv()
{
	return this->pv;
}

void Pokemon::setPv(int pv)
{
	this->pv = pv;
}

int Pokemon::getAtk()
{
	return this->atk;
}

void Pokemon::setAtk(int atk)
{
	this->atk = atk;
}

int Pokemon::getDef()
{
	return this->def;
}

void Pokemon::setDef(int def)
{
	this->def = def;
}

int Pokemon::getSpeed()
{
	return this->speed;
}

void Pokemon::setSpeed(int speed)
{
	this->speed = speed;
}

int Pokemon::getAtkSpec()
{
	return this->atkSpec;
}

void Pokemon::setAtkSpec(int atkSpec)
{
	this->atkSpec = atkSpec;
}

int Pokemon::getDefSpec()
{
	return this->defSpec;
}

void Pokemon::setDefSpec(int defSpec)
{
	this->defSpec = defSpec;
}

int Pokemon::getPrecision()
{
	return this->precision;
}

void Pokemon::setPrecision(int precision)
{
	this->precision = precision;
}

int Pokemon::getDodge()
{
	return this->dodge;
}

void Pokemon::setDodge(int dodge)
{
	this->dodge = dodge;
}

Sprite* Pokemon::getMiniThumb()
{
	return this->miniThumb;
}

void Pokemon::setMiniThumb(Sprite* sprite)
{
	this->miniThumb = sprite;
}

Sprite* Pokemon::getMediumThumb()
{
	return this->mediumThumb;
}

void Pokemon::setMediumThumb(Sprite* sprite)
{
	this->mediumThumb = sprite;
}

Sprite* Pokemon::getFightFace()
{
	return this->fightFace;
}

void Pokemon::setFightFace(Sprite* sprite)
{
	this->fightFace = sprite;
}

Sprite* Pokemon::getFightBack()
{
	return this->fightBack;
}

void Pokemon::setFightBack(Sprite* sprite)
{
	this->fightBack = sprite;
}



#pragma endregion
