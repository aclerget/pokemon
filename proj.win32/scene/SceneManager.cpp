#include "SceneManager.h"
#include "ui/CocosGUI.h"
#include "proj.win32/maps/MapManager.h"
#include "proj.win32/characters/character.h"

SceneManager::SceneManager()
{
}


SceneManager::~SceneManager()
{
}

cocos2d::Scene* SceneManager::create()
{
	auto scene = Scene::create();
	return scene;
}