#pragma once
#include "cocos2d.h"
#include <iostream>

using namespace cocos2d;
using namespace std;

#ifndef DEBUG_MODE
	#define DEBUG_MODE 1
#endif // !DEBUG_MODE


class Debug
{

public:
	Debug();
	~Debug();

	static void Log(std::string message);
};

