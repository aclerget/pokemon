#include "Debug.h"


Debug::Debug()
{
}


Debug::~Debug()
{
}

void Debug::Log(std::string message)
{
	auto scene = Director::getInstance()->getRunningScene();

	if (scene)
	{
		TTFConfig labelConfig;
		labelConfig.fontFilePath = "ui/fonts/pokemon_gb.ttf";
		labelConfig.fontSize = 13;
		labelConfig.glyphs = GlyphCollection::DYNAMIC;
		labelConfig.outlineSize = 0;
		labelConfig.customGlyphs = nullptr;
		labelConfig.distanceFieldEnabled = false;

		// create a TTF Label from the TTFConfig file.
		auto label = Label::createWithTTF(labelConfig, message, TextHAlignment::LEFT);
		label->setPosition(25, 80);
		scene->addChild(label);
	}

}
