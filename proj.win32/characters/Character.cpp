#include "Character.h"
#include "cocos2d.h"
#include <iostream>
#include "proj.win32/maps/MapManager.h"
#include "proj.win32/tools/Debug.h"

USING_NS_CC;

#pragma region CONSTRUCTORS

Character::Character(std::string name, Sprite *sprite)
{
	this->id = 0;
	this->name = name;
	this->sprite = sprite;
	this->isMine = false;
	this->isMoving = false;
	this->canMove = true;
	this->direction = CHARACTER_DIRECTION::DOWN;
	this->lastPosition = 1;
	this->speed = 2.f;
	this->collisionBox = new Rect(this->getPositionX(), this->getPositionY(), 3, 3);

	this->start();
}

Character::Character(int id, std::string name, Sprite *sprite)
{
	this->id = id;
	this->name = name;
	this->sprite = sprite;
	this->isMine = false;
	this->isMoving = false;
	this->canMove = true;
	this->direction = CHARACTER_DIRECTION::DOWN;
	this->lastPosition = 1;
	this->speed = 2.f;
	this->collisionBox = new Rect(this->getPositionX(), this->getPositionY(), 3, 3);

	this->start();
}

Character::~Character()
{
	delete(this->sprite);
	delete(collisionBox);
}

#pragma endregion

#pragma region METHODS

void Character::start()
{
	//call debug method
	this->debug();

	//init key direction
	keyDirection = 0;

	//set default no collision
	this->hasCollision = false;
	//call the event init
	this->initEvents();
	//call update method
	scheduleUpdate();
	//call animations method
	this->schedule(schedule_selector(Character::playMovementAnimation), 0.11f);
	// add the current sprite to child of the node
	this->addChild(this->sprite, TILE_PLAYER);

}

void Character::initEvents()
{
	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(Character::onKeyPressed, this);
	listener->onKeyReleased = CC_CALLBACK_2(Character::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithFixedPriority(listener, 1);
}

void Character::update(float delta)
{
	this->hasCollision = MapManager::getCollisionForCharacter(this);
	
	if (this->isMoving)
		addMovement(delta);
}

void Character::addMovement(float delta)
{
	if (hasCollision || !canMove)
		return;


	switch (this->direction)
	{
		case CHARACTER_DIRECTION::TOP:
			this->setPosition(Vec2(this->getPositionX(), (this->getPositionY() + speed)));
		break;
		case CHARACTER_DIRECTION::DOWN:
		default:
			this->setPosition(Vec2(this->getPositionX(), (this->getPositionY() - speed)));
		break;
		case CHARACTER_DIRECTION::LEFT:
			this->setPosition(Vec2(this->getPositionX() - speed, this->getPositionY()));
		break;
		case CHARACTER_DIRECTION::RIGHT:
			this->setPosition(Vec2(this->getPositionX() + speed, this->getPositionY()));
		break;
	}

}


void Character::playMovementAnimation(float delta)
{
	if (isMoving && canMove)
	{
		float newPosition(32.f * this->lastPosition);

		switch (this->direction)
		{
			case CHARACTER_DIRECTION::TOP:
				this->sprite->setTextureRect(Rect(newPosition, 0, 32.f, 32.f));
			break;
			case CHARACTER_DIRECTION::DOWN:
			default:
				this->sprite->setTextureRect(Rect(newPosition, 32.f, 32.f, 32.f));
			break;
			case CHARACTER_DIRECTION::LEFT:
				this->sprite->setTextureRect(Rect(newPosition, 64.f, 32.f, 32.f));
			break;
			case CHARACTER_DIRECTION::RIGHT:
				this->sprite->setTextureRect(Rect(newPosition, 96.f, 32.f, 32.f));
			break;
		}

		this->lastPosition++;
		if (this->lastPosition == 3)
			this->lastPosition = 0;
	}

}


void Character::debug()
{
	if (DEBUG_MODE == 1)
	{
		auto collisionRect = DrawNode::create();
		collisionRect->drawRect(
			Vec2(-7, -16),
			Vec2(7, -13),
			Color4F::RED);

		//we make margin for the rectangle is above the layer "above"
		this->addChild(collisionRect, TILE_ABOVE + 5, "Collision");
	}


}


#pragma endregion

#pragma region EVENTS

void Character::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	switch (keyCode)
	{
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			this->direction = CHARACTER_DIRECTION::TOP;
			this->isMoving = true;
			keyDirection++;
		break;
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			this->direction = CHARACTER_DIRECTION::DOWN;
			this->isMoving = true;
			keyDirection++;
		break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			this->direction = CHARACTER_DIRECTION::RIGHT;
			this->isMoving = true;
			keyDirection++;
		break;
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			this->direction = CHARACTER_DIRECTION::LEFT;
			this->isMoving = true;
			keyDirection++;
		break;
	}
}

void Character::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	switch (keyCode)
	{
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			keyDirection--;
		break;
	}

	if (keyDirection == 0)
		this->isMoving = false;
}

#pragma endregion

#pragma region GETTERS/SETTERS

Sprite* Character::getSprite()
{
	return this->sprite;
}

void Character::setSprite(Sprite *sprite)
{
	this->sprite = sprite;
}

bool Character::getIsMine()
{
	return this->isMine;
}

void Character::setIsMine(bool isMine)
{
	this->isMine = isMine;
}

bool Character::getIsMoving()
{
	return this->isMoving;
}

CHARACTER_DIRECTION Character::getDirection()
{
	return this->direction;
}

void Character::setDirection(CHARACTER_DIRECTION direction)
{
	this->direction = direction;
}

Rect* Character::getCollisionBox()
{
	return this->collisionBox;
}

#pragma endregion

